using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tech_test_payment_api.Entity;
using tech_test_payment_api.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace ApiVendas.src.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PedidosController : ControllerBase
    {
        private readonly Contexto _context;

        public PedidosController(Contexto context)
        {
            _context = context;
        }

        [HttpGet("ListarTodos")]
        public async Task<ActionResult<IEnumerable<Vendas>>> GetPedidos()
        {
            return await _context.Vendas.ToListAsync();
        }

        [HttpGet("BuscarPor{id}")]
        public async Task<ActionResult<Vendas>> GetPedido(int id)
        {
            var pedido = await _context.Vendas.FindAsync(id);

            if (pedido == null)
            {
                return NotFound();
            }

            return pedido;
        }


        [HttpPut("AlterarStatus{id}")]
        public async Task<IActionResult> PutPedido(int id, string status, Vendas pedido)
        {

            if (id == 0)
                return BadRequest("Campo id é necessário para buscar o pedido!");
            if (status == null)
                return BadRequest("Informe o novo status do pedido!");
            string[] statusBd1 = new string[2];

            Vendas pedidoBd = await _context.Vendas.FindAsync(id);

            if (pedidoBd.StatusDisponiveis.ToUpper() == "ENVIADO PARA TRANSPORTADORA" || pedidoBd.StatusDisponiveis.ToUpper() == "CANCELADO")
            {
                return BadRequest($"O pedido em tela não aceita modificações no seu status: {pedidoBd.StatusVendas}");
            }
            statusBd1 = pedidoBd.StatusDisponiveis.Split(",");
            statusBd1[0].Trim();
            statusBd1[1].Trim();


            if (status.ToUpper() != statusBd1[0].ToUpper() && status.ToUpper() != statusBd1[1].ToUpper())
            {
                return BadRequest($"O pedido em tela só aceita um dos status: {statusBd1[0]} ou {statusBd1[1]}");
            }
            DateTime data = DateTime.Now;


            data.ToShortDateString();
            pedidoBd.StatusVendas = status;
            pedidoBd.DataUltimaAtualizacao = data;
            if (status.ToUpper() == "PAGAMENTO APROVADO")
            {
                pedidoBd.StatusDisponiveis = "Enviado para Transportadora, Cancelado";
            }
            else if (status.ToUpper() == "ENVIADO PARA TRANSPORTADORA")
            {
                pedidoBd.StatusDisponiveis = "Entregue";
            }

            _context.Entry(pedidoBd).State = EntityState.Modified;


            try
            {
                await _context.SaveChangesAsync();
                return CreatedAtAction("GetPedido", new { id = pedido.Id }, pedido);

            }
            catch (DbUpdateConcurrencyException ex)
            {
                if (!PedidoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    return BadRequest(ex.ToString());
                }
                
            }

            //return NoContent() ;
        }



        // POST: api/Pedidos

        [HttpPost("NovoPedido")]
        public async Task<ActionResult<Vendas>> PostPedido(Vendas vendas, int vendedorId, string itens)
        {
            if (vendedorId == 0)
            {
                return BadRequest("Campo Id do vendedor é necessário!");
            }

            DateTime data = DateTime.Now;
            data.ToShortDateString();
            vendas.VendedorId = vendedorId;
            vendas.StatusVendas = "Aguardando Pagamento";
            vendas.StatusDisponiveis = "Pagamento Aprovado, Cancelado";
            vendas.DataVenda = data;
            vendas.DataUltimaAtualizacao = data;
            vendas.Itens = itens;

            try
            {
                _context.Vendas.Add(vendas);
                await _context.SaveChangesAsync();
                return CreatedAtAction("GetPedido", new { id = vendas.Id }, vendas);
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return BadRequest(ex.ToString());
            }

         }


        private bool PedidoExists(int id)
        {
            return _context.Vendas.Any(e => e.Id == id);
        }

         [HttpDelete("{id}")]
        public IActionResult Deletar(int id)
        {
            var pedidoBd = _context.Vendas.Find(id);
            if (pedidoBd == null) return NotFound();
            _context.Vendas.Remove(pedidoBd);
            _context.SaveChanges();
            return NoContent();
        }
    }
}